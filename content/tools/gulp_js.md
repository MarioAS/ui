---
title: "Gulp"
date: 2020-01-27
description: "Automatiza y mejora tu flujo de trabajo"
tags:
    - javascript
    - npm
    - www
    - automation
---
{{<figure src="https://gulpjs.com/img/gulp-white-text.svg" title="GulpJs" attrlink="https://gulpjs.com">}}
Automatiza y mejora tu flujo de trabajo, gulp es un kit de herramientas para automatizar tareas dolorosas que quitan mucho tiempo en el fluyo de trabajo del desarrollo

En un sistema operativo ya sea Windows, Linux o mac OS, se instala de manera rápida haciendo uso de npm (Node Package Manager), que es un administrador de paquetes para el entorno Node.js, que consiste en un cliente de líneas de comando, es el mayor repositorio de paquetes en Javascript

    $ npm install gulp-cli -g
    $ npm install gulp -D
    $ npx -p touch nodetouch gulpfile.js
    $ gulp --help

### Usando Gulpfile


    const { src, dest, parallel } = require('gulp');
    const pug = require('gulp-pug');
    const less = require('gulp-less');
    const minifyCSS = require('gulp-csso');
    const concat = require('gulp-concat');

    function html() {
    return src('client/templates/*.pug')
        .pipe(pug())
        .pipe(dest('build/html'))
    }

    function css() {
    return src('client/templates/*.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(dest('build/css'))
    }

    function js() {
    return src('client/javascript/*.js', { sourcemaps: true })
        .pipe(concat('app.min.js'))
        .pipe(dest('build/js', { sourcemaps: true }))
    }

    exports.js = js;
    exports.css = css;
    exports.html = html;
    exports.default = parallel(html, css, js);

#### Guía para desarrollar un website de producción con HUGO y GulpJs

[Hugo](https://gohugo.io "Website Hugo") es un generador de sitios web estáticos open source, en esta pequeña guía te vamos a llevar a conocer el proceso de construcción  de sitios web con hugo y [GulpJs](https://gulpjs.com "Website GulpsJs") como un assset pipeline para CSS, JS e imágenes.

#### Por qué Hugo?

La evolución que ha tenido internet ha dado nacimiento a nuevas tecnlogías, las cuales pueden llegar a ser de uso personal o empresarial, me refiero a las herramientas de administración de contenido, hace tiempo veíamos los primeros sitios webs que consistian sólo texto plano, pero lo importante era que la información se estaba compartiendo donde te encuentres y si tienes acceso a la red. Bueno vamos al caso, la idea de un buen sitio web no es sólo la estética y contenido de fondo, sino que en la plataforma en el cual está corriendo haga uso de pocos recursos y alguno de estos requerimientos:

1.  Rápida respuesta de las páginas.
2.  Una experiencia atractiva en el momento de editar y publicar contenido.
3.  Buena experiencia desarrollando.
4.  Código simple y fácil de mantener.
5. Seguridad sólida (Y no quiero hablar mal de wordpress).

Un gran punto a favor de las SSG(Static Site Generator) es que sólo hace uso de archivos estáticos (html, css, js, images), en lugar de una base de datos entera, como consecuencia gana velocidad en respuesta, seguridad y costo-beneficio.

### Qué necesitas?

Para mi es muy confortable usar línea de comandos, pues hugo y gulp.js no son la excepción, en la terminal con [Archlinux](https://www.archlinux.org/) como distribución favorita, pero además debemos por lo menos de manera básica tener conocimiento en CSS, SCSS, Javascript; Tener instalado [NodeJs](https://nodejs.org) y [npm](https:www.npmjs.com)

### Intalando Hugo

En Archlinux podemos instalar con el siguiente comando
```shell
sudo pacman -S hugo
```
ahora vamos a crear una carpeta y subcarpeta con el nombre `Code/MySite` en el directorio principal del usuario.
```shell
mkdir ~/Code/Mysite
cd ~/Code/Mysite
```
Como también puedes visitar la documentación en linea en el que también incluye la instalación [Hugo docs](https://gohugo.io/overview/installing/)

Situados en el folder creado, vamos por el primer comando hugo:

```go
hugo new site .
```
Nos creará la estructura básica y necesaria del sitio, la cual constará de 5 directorios y la configuración:
```shell
ls
archetypes config.toml content data layouts static
```

### Construyendo el Sitio

Es momento de deterne y pensar en la estructura del sitio, en nuestro caso tenemos uno bastente simple, un tipo de contenido - un artículo, y un simple artículo debe por lo menos contener metadatos, en nuestro casos serán pocos trozos:

1. Un Título
2. Una URL
3. Autor
4. Fecha de publicación
5. Descripción del SEO
6. Lista de recursos

Aparte de eso, es necesaria una página principal que muestre los últimos artículos.

### Agregando contenido

Agreguemos el primero artículo, con el siguiente comando y tomando en cuenta para futuras peticiones de ubicación de archivos, que se almacenan en el directorio `/content`, ruta relativa.

```go
hugo new articles/first.md
```
Se creará un nuevo archivo Markdown en la ruta relativa `/content/articles/first.md`, y si abrimos el archivo veremos que tenemos un formato repetivo en todos los casos que se utilize el comando anterior.
```markdown
---
title: "First"
date: 2020-01-28T13:01:11-04:00
draft: true
---
```
Algo a tomar encuenta es que **Hugo** cada vez que se cree  un artículo los metadatos determinados por los [**Front Matter Formats**](https://gohugo.io/content-management/front-matter/ "más información"), y por sus tokens de identificación ya se:

* TOML
    Se identifica al abrir y cerrar `+++`
* YAML
    Se identifica al abrir y cerrar `---`
* JSON
    Un único objeto rodeado de `{` y `}`, seguido de una nueva línea. 
* ORG
    No lo tomaré en cuenta por el momento, pero recuerda que puedes visitar [https://gohugo.io/content-management/front-matter/](https://gohugo.io/content-management/front-matter/)

### Arquetipos

Los arquetipos son necesarios para tener unas pautas de inicio en tanto con el formato, si verificamos en el momentos que se creo una nueva instancia del sitio, se crearon directorios una de ellas `Archetypes` y dentro de un archivo en Markdown, que lleva consigo el formato ya sea en **TOML, YAML o JSON**

### Estructura del Contenido

Por defecto la estructura de URL hará eco con la estructura de los directorios, así con URL of `http://example.com` este artículo se verá de la siguiente manera `http://example.com/articles/first/`


### Nota sobre Markdown

Markdown es realmente bonito para las personas acostumbradas a usar lenguajes de programación, una sintáxis bastante intuitiva, la manera que uso markdown es a través de un editor de código [Visual Studio Code](https://code.visualstudio.com/), pueden exister otros writer de Markdown, actualizaré este posts para nueva revisión. 

### Compilando Hugo

Ahora mismo tenemos el contenido pero no tenemos el sitio web, así que para compilar hugo es sólo necesario correr el comando:

```go 
hugo
```
Bien parece que fué rápido, en la terminal tendremos una impresión el cual nos muestra advertencias, errores, número de páginas, ect.
```
                   | EN  
+------------------+----+
  Pages            |  3  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  
```

Pero una vez compilado veremos nuevas carpetas

```
archetypes content layouts  resources  themes config.toml data public static
```
El directorio `/public` es la salida de hugo compilada.

Lo más seguro es que tengamos advertencias, o tambiém probablemente errores, vamos a ver el `log` al compilar **hugo**:

```
hugo ---verbose
```
Podemos ver el proceso al compilar, errores y advertencias, por ejemplo la falta la página de error ***404***, o la página ***home***

### Single Layout

Una manera de describir, es una pieza del contenido en el caso un artículo, estos son creados en archivos `html` para ser reconocido y la ruta debe estar en `/layouts/articles/single.html`, dentro colocaremos un diseño básico:

```hmtl
<!DOCTYPE html>
<html>
<head>
	<title>{{.Title}}</title>
	<meta charset="utf-8">
	<meta name="description" content="{{.Description}}">
</head>
<body>
	<main>
		<article>
			<h1>
				{{.Title}}
			</h1>
			{{.Content}}
		</article>
	</main>
</body>
</html>
```
Dentro de este documento html y sus etiquetas podemos ver algo nuevo y son las dobles llaves, pues esto nos facilita el acceso a herramientas y lógicas que los templates de Hugo nos proveen. En el layout de arriba accedemos a variables  que están al alcance del artículo.
Cada que hugo compila, para cada artículo se deberia de usar este layout, reemplazando las variables entre llaves dobles, hablemos del alcance de las variables `{{.}}` el actual alcance podrá luego ser accedida como propiedades, es decir luego se podrá acceder al titulo y contenido como propiedades  del scope.

### Página Home

La ruta para crear la página principal es  `/layout/index.html`

```
touch layouts/index.html
```
Aquí pondremos un contenido html como tal y verás las variables a las que estamos accediendo y que previamente fueron compiladas

```
<!DOCTYPE html>
<html>
<head>
	<title>{{.Title}}</title>
	<meta charset="utf-8">
	<meta name="description" content="{{.Description}}">
</head>
<body>
	<main>
		{{range first 10 .Data.Pages}}
		{{if eq .Type "articles"}}
			<article>
				<h1>
					<a href="{{.Permalink}}">
						{{.Title}}
					</a>
				</h1>
				{{.Summary}}
				<a href="{{.Permalink}}">
					Read the full article
			</article>
		{{end}}
		{{end}}
	</main>
</body>
</html>
```
Bueno aqui hay mucho que explicar, y vamos a realizarlo pero leyendo de derecha a izquierda.

```
{{range first 10 .Dates.Pages}}
```

`.Data.Pages` es una lista que contiene todas las páginas y contenido que existe en el sitio, esta pueda contener todos los artículos, inclusive las páginas estáticas como la de contacto y acerca de nosotros

`first` es una función auxiliar que hugo provee y la porciona a los numero de items de una lista, acepta argumentos y enteros y devuelve el valor

`range` es una funcion de GO para iterar a través de las listas.

`end` debe ser ejecuta en cada iteración.


```
{{if eq .Type "articles"}}
```
`eq` es una funcion de GO que acepta dos argumentos , prueba y retorna si es verdadero o falso

#### Página 404

Puede ser lo que tu desees poner cuando no se muestra algo correcto o ubo un error de compilación 

creado el layout 404 podemos volver a compilar hugo para que los cambios tengan efecto, algo que mencionar es que podemos correr el servidor hugo y tener una gran utilidad `live reload` que nos permite ver las actualizaciones de los cambios en tiempo real:

```
hugo server --watch --verbose
```
Nos mostrará una conección `http` seguida de la IP del localhost y un puerto.
```
http://localhost:1313/ 
```

### Directorio  `/static`

Cuando hugo creó por primera vez nuestro sitio, agregó 5 directorios a la raíz, vamos a ver el directorio `/static`, es bastante simple cualquier cosa que se ponga en el directorio `/static`, será compilado byte a byte  y puesto dentro de la carpeta pública, lo cual está bien para nuestros activos públicos, como CSS, Javascript e imágenes.
Vamos a dar un ejemplo creando un archivo css:
```
touch static/css/main.css 
```
y agregar

```
body {

	background: #FF0000;
}
```
también es necesaria incluir la hojas de estilo en la página principal, dentro de la etiqueta `<head>`
```
<link rel="stylesheet" href="/css/main.css" />
```
### Construyendo Gulp Pipeline

Vamos asumir que conoces Gulp, para que no sea más amplio partamos con esas referencias, vamos a configurar y compilar archivos `SCSS`, en un estado listo para producción, otra implementación es también la implementación del css en la última versión para los navegadores, tambień conocido como `Cache Busting`, de igual manera para tomar buena medida a los archivos `JS` e `imágenes`.
El resultado final que se quiere es que el conjunto de archivos activos se ubiquen en el directorio `static/` en el paso de compilación. Destinada como el directorio final de Gulp para mantener nuestros archivos de trabajo pre-procesado, creando dentro subdirectorios para `SCSS`, `JS` e `imágenes`. Vamos al rollo:

```
mkdir -p src/{scss,js,images}
```
### Instalando Gulp

Ok, primero lo primero, y hablamos de inicializar un nuevo proyecto `NPM` en el directorio raíz.

```s
npm init -y
npm install --save-dev gulp gulp-sass gulp-autoprefixer
```
### Creando tareas en GulpJS
```
touch gulpfile.js
```
En la cabecera del archivo, o sea la parte superior escribimos las dependencias
```js
var gulp 			= require("gulp"),
	sass			= require("gulp-sass"),
	autoprefixer	= require("gulp-autoprefixer")
```


>Nota: `--watch --verbose` se puede acortar con `-wv`
>
>Ref. [danbahrami.io](http://danbahrami.io/articles/building-a-production-website-with-hugo-and-gulp-js/ "Guia para desarrollar un website de producción con HUGO y GulpJs ")

