---
title: "TizenRT - Debugging"
date: 2020-01-30T21:07:22-04:00
draft: false
tags:
    - electronic
    - software
    - tizen
---

>Ref. [Tizen Studio Dev Tool](https://developer.tizen.org/ko/development/tizen-studio/platform-tools)

>Ref [Tizen Developers - Debugging](https://developer.tizen.org/ko/development/tizen-studio/rt-ide/getting-started/debugging-project?langredirect=1)