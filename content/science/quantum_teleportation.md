---
title: "Quantum Teleportation"
date: 2020-01-29T19:46:38-04:00
draft: false
tags: 
    - science
    - electronic
---

{{<figure src="https://img.europapress.es/fotoweb/fotonoticia_20191226103711_640.jpg">}}
>Ref.- [Science Alert](https://www.sciencealert.com/quantum-teleportation-reported-in-a-qutrit-for-the-first-time?fbclid=IwAR3aBSc-Nc6hGUE0MKGSc09z-nCMoP4p-FvMruMZQnISMTimUZAP9hADj_o)
>
>[Europa Press](https://www.europapress.es/ciencia/laboratorio/noticia-teleportacion-cuantica-informacion-dos-chips-programables-20191226103711.html)