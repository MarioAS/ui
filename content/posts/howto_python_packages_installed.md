---
title: "Maneras de ver lista de paquetes instalados en Python"
date: 2020-01-29T21:35:04-04:00
draft: false
toc: false
images:
tags:
  - python
  - howto
---

>Ref.- [LeemenDelOwitz](https://leemendelowitz.github.io/blog/how-does-python-find-packages.html)

Me he visto en la situación de que una vez instaldo python en linux, ver o saber la lista de paquetes instalados  que luego podrán ser importados, la situación se plantea del entorno de una manera global, esto aplicará a las versiones de python `2.xx` y `3.xx`. Tomar en cuenta que se pueden crear entornos virtuales, tal como [virtualenv](https://virtualenv.pypa.io/) o [conda](https://www.anaconda.com/)

En la terminal linux, en mi caso utilizando `tilix`, un emulador de terminal puedes ver la información [tilix install](https://github.com/gnunn1/tilix).

### [sys.path](https://docs.python.org/2/library/sys.html#sys.path)

Al realizar esta importación python lista los paquetes instalados en el sistema operativo:

En el caso para python `2.xx`
```python
$ python2
### dentro del interprete
>>> import sys
>>> print '\n'.join(sys.path)
### Output
/usr/lib/python27.zip
/usr/lib/python2.7
/usr/lib/python2.7/plat-linux2
/usr/lib/python2.7/lib-tk
/usr/lib/python2.7/lib-old
/usr/lib/python2.7/lib-dynload
/usr/lib/python2.7/site-packages
/usr/lib/python2.7/site-packages/gtk-2.0
/usr/lib/python2.7/site-packages/wx-3.0-gtk3
```
En el caso de python `3.xx`
```python
$ python3
### dentro del interprete
>>> help("modules")
Please wait a moment while I gather a list of all available modules...
```
Otra herramienta interesante es [pip](https://pip.pypa.io/), que esta disponible para ambas versiones de python `2.xx` y `3.xx`, en Archlinux, se encuentran en dos paquetes diferentes y de fácil instalación con `yay` a través de `pacman`:

```
$ yay -S python-pip  ### Para python 3
$ yay -S python2-pip ### Para python 2
```  
Instalado pip en nuestro sistema operativo, procedemos a verificar la lista de paquetes:
```
pip list
```