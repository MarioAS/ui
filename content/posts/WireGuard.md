---
title: "WireGuard"
date: 2020-02-11T09:24:19-04:00
draft: false
toc: false
images:
tags:
  - vpn
  - linux

---
## Qué es?

WireGuard® es un moderno VPN, rápido, multiplataforma, con lo último en criptpgrafía, simple, considerado mejor que OPENVPN, diseñado para propósitos generales desde interfaces embebidas  hasta supercomputadoras, ajustable para diferentes circunstancias, inicialmente lanzado para el kernel linux, pero ahora en configurable en la mayoria de los sistemas operativos más usados, ofreciendo soluciones más simples hasta soluciones en la industria.

## Uso
El objetivo de wireguard es ser simple en uso, fácil de configurar e implementar como SSH, y si hablamos de ssh, el comportamiento es el mismo con el intercambio de `Claves Públicas`, es resto manejado por `Wireguard`, hablabamos de la criptografía que hacia uso wireguard, tales coomo:
* [Noise protocol framework](http://www.noiseprotocol.org/) que también es usado por WhatsApp que está basado en el intercambio de llaves `Diffie-Hellman`
{{<figure src="/images/crypto/Diffie-Hellman_Key_Exchange.svg" width="150" height="200">}}
  >Ref. [UMN edu](https://conservancy.umn.edu/bitstream/handle/11299/107353/oh375mh.pdf?sequence=1&isAllowed=y)
>Ref. [www.wireguard.com](https://www.wireguard.com/)