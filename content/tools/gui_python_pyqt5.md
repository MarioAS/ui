---
title: "GUI en Python con PyQt5"
date: 2020-01-27
tags: 
    - python
author: "Mario Aguilar"
---
{{<figure src="https://pythonspot.com/wp-content/uploads/2016/07/mainmenu.png.webp" title="Iniciando a crear GUI con python">}}

### Código

    import sys
    from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel
    from PyQt5.QtGui import QIcon

    class App(QMainWindow):

        def __init__(self):
            super().__init__()
            self.title = 'PyQt'
            self.left = 10
            self.top = 10
            self.width = 440
            self.height = 280
            self.initUI()
        
        def initUI(self):
            self.setWindowTitle(self.title)
            self.setGeometry(self.left, self.top, self.width, self.height)
            
            label = QLabel('Python', self)
            label.move(50,50)
            
            label2 = QLabel('PyQt5', self)
            label2.move(100,100)
            
            label3 = QLabel('Examples', self)
            label3.move(150,150)
            
            label4 = QLabel('pytonspot.com', self)
            label4.move(200,200)
            
            self.show()

    if __name__ == '__main__':
        app = QApplication(sys.argv)
        ex = App()
        sys.exit(app.exec_())

>Ref. [pythonspot.com/category/pyqt5/page/2/](https://pythonspot.com/category/pyqt5/page/2/)