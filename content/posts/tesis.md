---
author:
- Mario Aguilar
bibliography:
- biblio.bib
title: Antítesis
---

El Documento, Formato
=====================

En mi Universidad tenemos un formato de tesis que hasta el momento está
estandarizado en la norma APA. Entonces tomaremos ciertas
consideraciones respecto al formato: Las Normas APA invitan a la
eliminación de las redundancias, ambigüedades, generalidades que
entorpezcan la comprensión. La extensión adecuada de un texto es la
rigurosamente necesaria para decir lo que deba ser dicho.

-   **Tipo de Papel** - Tamaño: Carta (Letter)/papel 21.59 cm. x
    27.94 cm. (8 1/2\" x 11\")

-   **Márgenes** - Hoja: 2.54 cm. (1 pulgada) en cada borde de la hoja
    (Superior, Inferior, Izquierda, Derecha).\
    - Sangría: Es necesario dejar 5 espacios. o $0,5$ cm

-   **Fuente o Tipo de Letra** - Fuente: Times New Roman\
    - Tamaño: 12 pts.\
    - Alineamiento: Izquierda.\
    - Interlineado: 2.\

-   **Numeración de Páginas**

-   **Abreviaciones**

Agradecimientos
===============

Después de algunos años de estudios, me veo en la necesidad de agradecer
a las personas que confiaron en mi muy sin importar el momento, lugar o
situación en la que se encontrase en cualquier pasaje de sus vidas, soy
una persona que prefiere evitarse esos momentos posteriores al logro
\"Celebración\", por que estoy seguro que hay mucho más por aprender,
dar aportes valiosos a nivel académico, cultural, social y científico.
Pero si tendría que agradecer muy satisfactoriamente diría, Gracias
Madre María Consuelo Salazar Cespedes, Gracias a mi Familia, Gracias a
los Buenos Docentes, como sé trata de un previo agradecimiento sólo
teniendo en cuenta el tiempo transcurrido hasta la fecha.

Previos
=======

*sección es explicativa dando referencia a las herramientas utilizadas y
que se utilizarán en el transcurso:*

La filosofía de inicio es utilizar todas las herramientas que sean *Open
Source* desde inicio hasta el final, es cierto que en el camino nos
encontraremos con tecnologías que son muy avanzadas y que son de pagos,
implicando así las cuestión de usarlas o no, sabemos que todo lo que
pagas de alguna u otra manera de automatiza algún trabajo o servicio, o
de otro simplemente no tiene pagar. El trabajo de Ingeniería implica dar
algún tipo de solución, nuestro objetivo de los muchos es utilizar
tecnología que está al alcance de cualquier persona, y facilitar a la
hora de elegir con qué trabajar, o con que iniciar un nuevo
emprendimiento tecnológico.

Dado que nuestras tecnologías utilizadas serán muy factibles y/
mantienen una amplia documentación, Gracias a la comunidad. En cuanto a
la metodología de investigación que se requerirá o se planea implementar
se optará por lo más accesible, puede que en proceso exista alguna
alternativa, teniendo como finalidad el dominarlas.

Por qué usar Software Libre y Open Source
-----------------------------------------

Herramientas
------------

1.  **[GNU/Linux]{.sans-serif}** La Fundación GNU

2.  **LaTeX** Es una herramienta disponible de manera de pública como
    software libre, en definición un sistema de composición tipografía
    de alta calidad para la producción de documentación científica y
    técnica,LaTeXes de hecho un estándar para la comunicación y
    publicación de documentos científicos. A la fecha LaTeXse encuentra
    en la versión[@LATEX]

Introducción
============

El planteamiento teórico con fundamentos relacionados en el campo de la
salud, la idea principal es tomar herramientas Free Software y con la
misma filosofía de la Free Software Fundation FSF. Lo que significa que
los usuarios tienen la libertad de ejecución, estudiar, cambiar, y
distribuir.

Marco Teórico
=============

Objetivos
=========

Generales
---------

-   Aprender Linux

-   Utilizar y colaborar al Free Software y Open Source

Específicos
-----------

Conceptos
=========

Conclusión
==========
