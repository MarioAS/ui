+++
title = "About"
date = "2020-01-03"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Mario Aguilar"
+++

Mi nombre es **Mario Antonio Aguilar Salazar**, soy de **de Bolivia**, estusiasta del software libre, el mundo de la electrónica, estudio en ramas de la física, matemáticas y química, las innovaciones tecnológicas, e implementar todas esas herramientas en alguna soluciones beneficiosa a nivel social. a la actual fecha 6 de enero de 2020 tengo la edad de 25 años.
r
Agradecer a **NOW.SH** por facilita  a las personas como yo que de momento no tenemos la posibilidad de adquirir un dominio, este hosting gratuito, el cual será utilizado para que este servidor realice blogging, pero además compartir información, teniendo en cuenta la mentalidad **Lo que se aprende se comparte.**

De igual manera agradecer a [Hugo](https://gohugo.io/) un Generador de Sitios Estáticos "SSG". El cual está escrito en el lenguaje de programación [Go](https://golang.org/). 

Este blog está dedicado a personas que necesiten información al respecto con los lenguajes de programación Python, Go, Javascript, C++, C. Tomar en cuenta que la forma en como se lo utilize dependerá de la solución que se esté planteando. Se tratará de explicar explícitamente de la mejor forma.

`
Ejemplo: Solución electrónica con inteligencia artificial en un procesador con trabajo dedicado, optimizando las tareas.
`