---
title: "LinuxVServer"
date: 2020-02-11T13:40:41-04:00
draft: false
tags:
    - linux
    - virtualitation
---

## Inicio

## Linux-VServer

Proporciona virtualización para sistemas GNU/Linux, esto se logra mediante el aislamiento a nivel de kernel, permite ejecutar multiples unidades virtuales a la vez, esas unidades están suficientemente aisladas para garantizar la seguridad requerida, pero utiliza eficientemente los recursos disponibles, ya que se ejecutan en el mismo núcleo.

## Tipos de Virtualización

* Emulation

Un software emulador, permite al programa de computadora correr una plataforma (Arquitectura de computora y/o sistema operativo) aparte de aquel para el cual fueron escritos originalmente, es diferente a simulación que sólo intenta reproducir el comportamiento de un programa, la emulación intenta modelar a varios grados los estados del dispositivo que está emulado. La máquina virtual simula el hardware completo permitiendo a un OS sin modificar para correr una CPU completamente diferente.

* Paravirtualización

Es una técnica de virtualización que presenta una interfaz de software para máquina virtuales que es similar pero no identico

>Ref. [Linux-vserver](http://linux-vserver.org/Overview)

>Ref. [linux-vserver.org/](http://linux-vserver.org/Welcome_to_Linux-VServer.org)
>Ref. [RedHat Containers](https://www.redhat.com/es/topics/containers)