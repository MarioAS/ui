---
title: "Interferencia_electromagnetica"
date: 2020-02-09T22:47:37-04:00
draft: false
---

### Definición

### Electromagnetismo

Es una rama de la física o ingenieria eléctrica en el cual los fenómenos eléctricos y magnéticos son estudiados 


La **Interferencia electromagnética** (EMI = Electromagnetic Interference) o también llamado **Interferencia de Radiofrecuencia** cuando está en el espectro de la radio frecuencia

Cualquier perturbación electromagnética que interrumpa, obstruya o degrade el rendimiento de los equipos electricos y electrónicos, ya sea intencionalmente inducido, o no intencional, como resultado de emisión y respuestas espurias, producto de la intermodulación

Ref. [DAU EDU - EMI](https://www.dau.edu/cop/e3/Pages/Topics/Electromagnetic%20Interference%20EMI.aspx)

### Métodos para la reducción de EMI

Los métodos más comunes de reducción de EMI que incluyen los más adecuados diseños de circuitos, protección, toma de tierra, filtrado, aislamiento, seperación, orientación y cancelación de ruído.

>Ref. [Hub Pages EMI](https://hubpages.com/education/The-Basis-of-EMI-Electromagnetism)
>Ref. [Micro Magnet FSU edu Optic](https://micro.magnet.fsu.edu/optics/lightandcolor/filter.html)
>Ref. [MDPI](https://www.mdpi.com/2079-9292/8/5/499)
>Ref. [SlideShare EMI](https://www.slideshare.net/sabeelirshad/electromagnetic-interference-electromagnetic-compatibility)