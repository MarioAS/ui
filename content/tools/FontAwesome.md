+++
title= "Font Awesome"
date= "2020-01-03"
aliases = ["about-us","about-hugo","contact"]
[ author ]
    name = "Mario Aguilar"
+++

### [Font Awesome Free](https://fontawesome.com)

El Kit de Herramientas icónicos más famosos en el internet SVG, Fuentes y CSS, diseñado y construido desde cero, para características como ligaduras de fuentes de iconos, un marco SVG, paquetes oficiales de NPM para bibliotecas frontales populares como React y acceso a un nuevo CDN.

### Dónde usar Font Awesome?

Las respuesta más rápida es: *se puede utilizar en la red*, por nombrar:

*   Con SVG "Scalable Vector Graphics"
*   Con JavaScrip "Lenguaje de programación"
*   Con Fuentes basados en la Web
*   Con CSS "Cascading Style Sheet"

### Dónde lo utilice?

Fué utilizado para crear un template

