---
title: "droidVNCServer"
date: 2020-01-27
tags:
    - android
---

## El proyecto VNC para dispositivos android

Es un proyecto libre listo para compilar, tomando en cuenta primero los requisitos:
>Cosiste en tres principales modulos: **The Daemon**, **Librerias de enlaces** y la **GUI**

#### The Daemon 
    
Provee las funcionalidades del servidor VNC, Injets Inputs/Touch Events, Manejo del portapapeles, etc.

#### Librerias de Enlaces

Compilado contra el AOSP(Android Open Source Projects)
#### GUI

El manejo de la GUI completamente amigable, conectando con el **Daemon** usando localemente IPC

## Compiled

```
-------------- Compile C daemon ---------------------
On project folder:
  $ ndk-build
  $ ./updateExecsAndLibs.sh

-------------- Compile Wrapper libs -----------------
  $ cd <aosp_folder>
  $ . build/envsetup.sh
  $ lunch
  $ ln -s <droid-vnc-folder>/nativeMethods/ external/

To build:
  $ cd external/nativeMethods
  $ mm .
  $ cd <droid-vnc-folder>
  $ ./updateExecsAndLibs.sh

-------------- Compile GUI------- -------------------
Import using eclipse as a regular Android project
```