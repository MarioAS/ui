+++
title= "New: La NASA obtiene una rara vista de la superficie de un exoplaneta rocoso"
date= "2020-01-04"
[ author ]
    name = "Mario Aguilar"
+++

La NASA obtiene una rara vista de la superficie de un exoplaneta rocoso

Exoplaneta LHS 3844b Exoplaneta LHS 3844b - NASA/JPL-CALTECH/R. HURT (IPAC) MADRID, 19 Ago. (EUROPA PRESS) - Un nuevo estudio con datos del Telescopio Espacial Spitzer de la NASA proporciona una visión rara de las condiciones en la superficie de un planeta rocoso que orbita una estrella más allá del Sol. El estudio, publicado este 19 de agosto en la revista Nature, muestra que la superficie del planeta puede parecerse a la de la Luna de la Tierra o Mercurio: es probable que ...




