---
title: "LinuxPermissions"
date: 2020-02-11T19:01:10-04:00
draft: false
toc: false
images:
tags:
  - linux
  - os
---

## Introducción

Una de las características de los sistemas operativos es que pueden llegar a ser multiusuarios, y linux no es la excepción así que vamos a aprender a como crear usuarios, dar permisos, crear grupo de usuarios, dar permisos a los grupos de usuarios, asi que a la marcha.

### Admin - Root - SuperUser

En pocas palabras en un sistema operativo que es instalado desde cero, en uno de los pasos es la creación  del usuario por defecto o como se ve en los acrónimos del titulo.

